import java.util.Collections;
import java.util.Scanner;
import java.util.ArrayList;

public class QuidScanner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        QuidFunctions quidFunctions = new QuidFunctions();

        String result;
        ArrayList<String> houses = new ArrayList<>();
        String word;
        int gCount = 0;
        int rCount = 0;
        int hCount = 0;
        int sCount = 0;
        while (scanner.hasNextLine()) {
            word = scanner.nextLine();
            result = quidFunctions.formatLine(word);
            houses.add(result);
        }
        Collections.sort(houses);
        for (String h : houses) {
            switch (h) {
                case "Gryffindor":
                    gCount++;
                    break;
                case "Hufflepuff":
                    hCount++;
                    break;
                case "Ravenclaw":
                    rCount++;
                    break;
                case "Slytherin":
                    sCount++;
                    break;
                default:
                    System.out.println("this isn't supposed to happen");
            }
        }
        if (gCount == 7 && hCount == 7 && rCount == 7 && sCount == 7) {
            System.out.println("List complete, let’s play quidditch!");
            System.exit(0);
        }

        if (gCount < 7) {
            System.out.println("Gryffindor does not have enough players.");
        }
        else {
            System.out.println("Gryffindor has too many players.");
        }

        if (hCount < 7) {
            System.out.println("Hufflepuff does not have enough players.");
        }
        else {
            System.out.println("Hufflepuff has too many players.");
        }

        if (rCount < 7) {
            System.out.println("Ravenclaw does not have enough players.");
        }
        else {
            System.out.println("Ravenclaw has too many players.");
        }

        if (sCount < 7) {
            System.out.println("Slytherin does not have enough players.");
        }
        else {
            System.out.println("Slytherin has too many players.");
        }
    }
}