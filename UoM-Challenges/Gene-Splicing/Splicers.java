import java.util.Scanner;

public class Splicers {
    Scanner scanner = new Scanner(System.in);

    public int getNumCases() {
        int num_of_cases;
        String first_line;
        first_line = scanner.nextLine();
        num_of_cases = Integer.parseInt(first_line);
        return num_of_cases;
    }

    public String getNextLine() {
        String line;
        line = scanner.nextLine();
        return line;
    }
}
