import java.util.Scanner;

public class CipherFunctions {
    Scanner scanner = new Scanner(System.in);

    public String getStringLine() {
        String line;
        line = scanner.nextLine();
        return line;
    }

    public int getKey() {
        String line;
        int key;
        line = getStringLine();
        key = Integer.parseInt(line);
        return key;
    }

    public String encrypt(String line, int key) {
        final String LOWER_ALPHABET = "abcdefghijklmnopqrstuvwxyz";
        final String UPPER_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int pos;
        int added;
        char letter;
        String result = "";

        for (int i = 0; i < line.length(); i++) {
            letter = line.charAt(i);
            if(Character.isLowerCase(letter)){
                pos = LOWER_ALPHABET.indexOf(letter);
                added = key + pos;
                added %= 26;
                letter = LOWER_ALPHABET.charAt(added);
                result += letter;
            }
            else if(Character.isUpperCase(letter)) {
                pos = UPPER_ALPHABET.indexOf(letter);
                added = key + pos;
                added %= 26;
                letter = UPPER_ALPHABET.charAt(added);
                result += letter;
            }
        }
        return result;
    }
}
