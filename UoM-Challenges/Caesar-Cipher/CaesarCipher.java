public class CaesarCipher {
    public static void main(String[] args) {
        CipherFunctions cipherFunc = new CipherFunctions();
        int key = cipherFunc.getKey();
        String line = cipherFunc.getStringLine();
        while (true){
            line = cipherFunc.encrypt(line, key);
            System.out.println(line);
            line = cipherFunc.getStringLine();
            if (line.equals("STOP")) {
                break;
            }
        }
    }
}
