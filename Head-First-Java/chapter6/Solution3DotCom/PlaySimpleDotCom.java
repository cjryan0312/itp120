public class PlaySimpleDotCom {
    public static void main(String[] args) {
        SimpleDotCom game = new SimpleDotCom();
        CliReader reader = new CliReader();

        int removed = 0;
        int numOfGuesses = 0;
        int num1 = (int) (Math.random() * 5);
        int num2 = num1 + 1;
        int num3 = num1 + 2;
        int[] locations = {num1, num2, num3};
        game.setLocationCells(locations);

        boolean running = true;
        while (running) {
            String guess = reader.input("Enter a number: ");
            String result = game.checkYourself(guess);
            String numOfHits = game.returnNumOfHits();
            numOfGuesses++;
            if (result.equals("hit") && numOfHits.equals("1")) {
                if (Integer.parseInt(guess) == num1) {
                    int[] locations1 = {num2, num3};
                    game.setLocationCells(locations1);
                    removed = 0;
                }
                else if (Integer.parseInt(guess) == num2) {
                    int[] locations1 = {num1, num3};
                    game.setLocationCells(locations1);
                    removed = 1;
                }
                else {
                    int[] locations1 = {num1, num2};
                    game.setLocationCells(locations1);
                    removed = 2;
                }
            }

            if (result.equals("hit") && numOfHits.equals("2")) {
                if (removed == 0) {
                    if (Integer.parseInt(guess) == num2) {
                        int[] locations2 = {num3};
                        game.setLocationCells(locations2);
                    }
                    else if (Integer.parseInt(guess) == num3) {
                        int[] locations2 = {num2};
                        game.setLocationCells(locations2);
                    }

                }

                if (removed == 1) {
                    if (Integer.parseInt(guess) == num1) {
                        int[] locations2 = {num3};
                        game.setLocationCells(locations2);
                    }
                    else if (Integer.parseInt(guess) == num3) {
                        int[] locations2 = {num1};
                        game.setLocationCells(locations2);
                    }
                }

                if (removed == 2) {
                    if (Integer.parseInt(guess) == num1) {
                        int[] locations2 = {num2};
                        game.setLocationCells(locations2);
                    }
                    else if (Integer.parseInt(guess) == num2) {
                        int[] locations2 = {num1};
                        game.setLocationCells(locations2);
                    }
                }
            }
            if (result.equals("kill")) {
                running = false;
                System.out.println("You took " + numOfGuesses + " guesses");
            }
        }
    }
}
