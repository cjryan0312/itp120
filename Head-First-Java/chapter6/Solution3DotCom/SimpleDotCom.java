public class SimpleDotCom {
    int[] locationCells;
    int numOfHits;

    public void setLocationCells(int[] locs) {
        locationCells = locs;
    }

    public String checkYourself(String stringGuess) {
        int guess = Integer.parseInt(stringGuess);
        String result = "miss";
        for (int cell : locationCells) {
            if (guess == cell) {
                result = "hit";
                numOfHits++;
            }
        }
        if (numOfHits == 3) {
            result = "kill";
        }
        System.out.println(result);
        return result;
    }

    public String returnNumOfHits() {
        return Integer.toString((numOfHits));
    }
}
