class Clock {
    String time;
    void setTime(String t) {
        time = t;
    }
    // original had void getTime, to return something it cannot be void
    String getTime() {
        return time;
    }
}
