class DogTestDrive {
    public static void main(String[] args) {
        Dog one = new Dog();
        one.size = 70;
        one.name = "George";
        Dog two = new Dog();
        two.size = 8;
        Dog three = new Dog();
        three.size = 35;

        one.bark();
        two.bark();
        three.bark();
        one.multiBark(3);
        String words = one.talk();
        System.out.println(words);
        one.foodEaten(1, 1);
    }
}