class Dog {
    int size;
    String name;

    void bark() {
        if (size > 60) {
            System.out.println("Wooof! Wooof!");
        }
        else if (size > 14) {
            System.out.println("Ruff Ruff");
        }
        else {
            System.out.println("Yip Yip");
        }
    }
    void multiBark(int numBarks) {
        int num = numBarks;
        while (numBarks > 0) {
            System.out.println("ruff");
            numBarks = numBarks - 1;
        }
        System.out.println(name + " barked " + num + " times");
    }
    String talk() {
        return "dogs cant talk";
    }
    void foodEaten(int morningCups, int nightCups) {
        System.out.println(name + " ate " + morningCups + " cup in the morning and " + nightCups + " cup at night");
    }
}