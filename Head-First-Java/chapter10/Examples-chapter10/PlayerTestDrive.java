public class PlayerTestDrive {
    public static void main(String[] args) {
        // will print 0 because no players have been initiated
        System.out.println(Player.playerCount);
        Player one = new Player("Brock Lee");
        // will print 1 because of Brock Lee being initiated
        System.out.println(Player.playerCount);
    }
}
