public class Duck {
    private int size;
    private static int duckCount = 0;

    public Duck() {
        duckCount++;
    }

    public void setSize(int a) {
        size = a;
    }
    public int getSize() {
        return size;
    }
}
