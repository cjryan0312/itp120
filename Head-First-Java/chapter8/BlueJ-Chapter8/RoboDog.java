
/**
 * Write a description of class RoboDog here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class RoboDog extends Robot implements Pet {
    public void move(){
        System.out.println("RoboDog leaps forward");
    }
    public void play() {
        System.out.println("RoboDog wants to play fetch");
    }
    public void beFriendly(){
        System.out.println("RoboDog runs in circles around you");
    }
}
