public class Tiger extends Feline {
    public void eat() {
        System.out.print("Tiger is eating meat");
    }
    
    public void makeNoise() {
        System.out.print("meow-roar");
    }
}
