
/**
 * Write a description of class Dog here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Dog extends Canine implements Pet{
    public void eat() {
        System.out.println(name + " is eating dog food");
    }
    public void makeNoise() {
        System.out.println("woof woof");
    }
    public void play() {
        System.out.println(name + " wants to play fetch");
    }
    public void beFriendly(){
        System.out.println(name + " licks you");
    }
    public void sharePhoto() {
        System.out.println("A photo of " + name + " can be found at: " + picture);
    }
}
