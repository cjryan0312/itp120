public class George {
    public static void main(String[] args){
        Dog george = new Dog();
        george.setName("George");
        george.setPicture("https://imgur.com/uJZRYso");
        george.setFood("dog food");
        george.setHunger(0);
        george.setBoundaries("None");
        george.setLocation("digging in the recycling bin");
        
        george.eat();
        george.makeNoise();
        george.play();
        george.beFriendly();
        george.sleep();
        george.sharePhoto();
    }
}
