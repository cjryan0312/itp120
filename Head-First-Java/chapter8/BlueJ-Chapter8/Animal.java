
/**
 * Abstract class Animal - This is the animal abstract class, for a real life
 * example, go outside.
 *
 * @author Christopher Ryan
 * @version ¯\_(ツ)_/¯
 */
public abstract class Animal {
    public String picture;
    private String food;
    private int hunger;
    private String boundaries;
    private String location;
    public String name;
    
    public void setPicture(String P) {
        picture = P;
    }
    
    public void setFood(String F) {
        food = F;
    }
    
    public void setHunger(int H) {
        hunger = H;
    }
    
    public void setBoundaries(String B) {
        boundaries = B;
    }
    
    public void setLocation(String L) {
        location = L;
    }
    
    public void setName(String N) {
        name = N;
    }
    
    public abstract void eat();
    
    public abstract void makeNoise();
    
    public void sleep() {
        System.out.println("zzzzzz");
    }
    
    public abstract void roam();
}
