public class TestObject{
    public static void main(String[] args) {
        Dog a = new Dog();
        Cat c = new Cat();
        
        System.out.println(c.getClass());
        System.out.println(a.getClass());
        
        System.out.println("Are these two objects equal?");
        if(a.equals(c)) {
            System.out.println("true");
        } 
        else {
            System.out.println("false");
        }  
        
        System.out.println("Cat hashcode and to string");
        System.out.println(c.hashCode());
        System.out.println(c.toString());
    }    
}