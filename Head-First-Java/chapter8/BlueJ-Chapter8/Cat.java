public class Cat extends Feline implements Pet {
    public void eat() {
        System.out.print("cat is eating cat food");
    }
    public void makeNoise() {
        System.out.print("meow");
    }
    public void play() {
        System.out.println("cat wants to play fetch");
    }
    public void beFriendly(){
        System.out.println("cat cuddles up beside you");
    }
}