public class Lion extends Feline {
    public void eat() {
        System.out.print("Lion is eating meat");
    }
    
    public void makeNoise() {
        System.out.print("roar");
    }
}
