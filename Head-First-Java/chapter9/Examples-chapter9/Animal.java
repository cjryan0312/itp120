public abstract class Animal {
    private String name;
    
    public String getName(){
        return name;
    }
    
    public Animal(String theName) {
        System.out.println("making animal");
        name = theName;
    }
}