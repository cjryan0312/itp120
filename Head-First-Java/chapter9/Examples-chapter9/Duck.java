public class Duck{
    int size;
    
    public Duck() {
        super();
        System.out.println("quack, making default duck");
        size = 24;
        System.out.println("Size is " + size);
    }
    
    public Duck(int duckSize) {
        super();
        System.out.println("quack, making duck");
        size = duckSize;
        System.out.println("Size is " + size);
    }
}
