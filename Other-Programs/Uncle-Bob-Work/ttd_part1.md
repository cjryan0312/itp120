#### 1. What are the Three Laws of Test-driven Development?
Write NO production code, except to pass a failing test

Write only enough of a test to demonstrate a failure 

Write only enough production code to pass the test


#### 2. Explain the Red -> Green -> Refactor -> Red process.
An approach to TDD where you write tests first, before production code.

#### 3. What are the three characteristics Uncle Bob lists of rotting code?
Rigid, fragile, and immobile.

#### 4. Explain how fear promotes code rot and why the fear exists in the first place. How does TDD help us break this vicious cycle?
Fear promotes code rot because if you try to fix a bad piece of code, it might break, which causes fear of changing the code so it doesn’t work, which causes the rotting code to rot even more and the cycle repeats. TDD breaks this vicious cycle because it allows you to change and lets you how it affects it.


#### 5. Uncle Bob mentions FitNesse as an example of a project that uses TDD effectively. What is FitNesse? What does it do?
FitNesse is a web based, open source project, automated testing tool for software.

#### 6. What does Uncle Bob say about a program with a long bug list? What does he say this comes from?
A program with a long bug list comes from carelessness and irresponsibility.

#### 7. What two other benefits does Uncle Bob say you get from TDD besides a reduction in debugging time?
Loss of fear of making changes to the code and keeping defects under control.